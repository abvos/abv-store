# abv-store

[AbvOS](https://tondy67.github.io/abvos/) store using [Stripe](https://stripe.com/) as payment processor.

[Instructions](https://github.com/tondy67/abv-store) how to start your own store. 

### Environment variables
```
export STRIPE_PK=your_stripe_public_key
export STRIPE_SK=your_stripe_secret_key 
export ABV_TOS=link_to_legal_docs
```
Set your [list of products](https://dashboard.stripe.com/products)

###[Live demo](https://abvos.xyz/)


License: [MIT](./LICENSE)
