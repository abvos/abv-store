/**
 * Abvos store
 * https://github.com/tondy67/abv-store
 */
"use strict";

const AbvStore = require('./lib/AbvStore.js');

module.exports = AbvStore;
